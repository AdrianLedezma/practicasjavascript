const MoTabla = document.getElementById('MTabla');

MoTabla.addEventListener('click', function(){
    Signos();
    Multiplicar();  
})

function Signos(){
    var imagenesPor = document.querySelectorAll('#por');
    var imagenesIgual = document.querySelectorAll('#igual');
    var imagenesNum = document.querySelectorAll('#num');
    let numero = document.getElementById('cmbNumeros').value;
    var imagenesNum2 = document.querySelectorAll('#num2');

    for (var i = 0; i < imagenesPor.length; i++) {
        imagenesPor[i].src = "/img/img-tablas/x.png";
    }

    for (var i = 0; i < imagenesIgual.length; i++) {
        imagenesIgual[i].src = "/img/img-tablas/=.png";
    }

    for (var j = 0; j <= 10; j++) {
        if (j === parseInt(numero)) {
            for (var i = 0; i < imagenesNum.length; i++) {
                imagenesNum[i].src = "/img/img-tablas/" + j + ".png";
            }
        }
    }

    for (var i = 0; i < imagenesNum2.length; i++) {
        imagenesNum2[i].src = "/img/img-tablas/" + (i+1) + ".png";
    }  
}

function Multiplicar(){
    var Numero = parseInt(document.getElementById("cmbNumeros").value);
    
    for(let i = 1; i <= 10; i++){
        var result = Numero * i;
        var resultString = result.toString();
        var digito1 = resultString.charAt(0);
        var digito2 = resultString.charAt(1);
        var digito3 = resultString.charAt(2);
        document.getElementById("res"+i).src = "/img/img-tablas/" + digito1 + ".png";
        if(digito2 === ''){
            document.getElementById("res"+i+"2").src = "/img/img-tablas/imagen.png";
        }else{
            document.getElementById("res"+i+"2").src = "/img/img-tablas/" + digito2 + ".png";
        }

        if(digito3 === '0'){
            document.getElementById("res"+i).src = "/img/img-tablas/10.png";
        }
    }
}
