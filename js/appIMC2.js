
function CalIMC(){
    var altura = parseFloat(document.getElementById("txtAltura").value);
    var peso = parseFloat(document.getElementById("txtPeso").value);
    var genero = document.querySelector('input[name="sexo"]:checked');
    var edad = parseInt(document.getElementById("txtEdad").value);

    if (isNaN(altura) || isNaN(peso) || genero === null || isNaN(edad)) {
        alert("Por favor, complete todos los campos requeridos.");
        return;
    }
        altura = altura * altura;
        var imc = peso / altura;
        imc = imc.toFixed(2);

        document.getElementById("txtResultado").value = imc;


        CalcularCaloriasRecomendas(genero.value,edad,peso);

        ImagenRotativa(genero.value,imc);
}

function LimpiarParametros(){
    var img = document.querySelector(".imagen");
    document.getElementById('txtEdad').value = "";
    document.getElementById('txtPeso').value = "";
    document.getElementById('txtAltura').value = "";
    document.getElementById('txtResultado').value = "";
    document.getElementById('txtCalorias').value = "";
    img.style.backgroundImage = "url('')";
}

function ImagenRotativa(gen, imc) {
    var imgact = "";
    var img = document.querySelector(".imagen");
    
    if (gen === "hombre") {
        imgact = getHombreImagen(imc);
    } else {
        imgact = getMujerImagen(imc);
    }
    img.style.backgroundImage = "url('" + imgact + "')";
}

function getHombreImagen(imc) {
    if (imc < 18.5) {
        return "/img/01H.png";
    } else if (imc < 25) {
        return "/img/02H.png";
    } else if (imc < 30) {
        return "/img/03H.png";
    } else if (imc < 35) {
        return "/img/04H.png";
    } else if (imc < 40) {
        return "/img/05H.png";
    } else {
        return "/img/06H.png";
    }
}

function getMujerImagen(imc) {
    if (imc < 18.5) {
        return "/img/01M.png";
    } else if (imc < 25) {
        return "/img/02M.png";
    } else if (imc < 30) {
        return "/img/03M.png";
    } else if (imc < 35) {
        return "/img/04M.png";
    } else if (imc < 40) {
        return "/img/05M.png";
    } else {
        return "/img/06M.png";
    }
}

function CalcularCaloriasRecomendas(generos,edad,peso){ 
    var caloins;
    var calorias;
    if(generos === "hombre"){
        if(edad<18){
            calorias = (17.686*peso) + 658.2;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        } else if(edad<30){
            calorias = (15.057*peso) + 692.2;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        } else if(edad<60){
            calorias = (11.472*peso) + 873.1;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        } else if(edad>=60){
            calorias = (11.711*peso) + 587.7;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        }
    } else{
        if(edad<18){
            calorias = (13.384*peso) + 692.6;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        } else if(edad<30){
            calorias = (14.818*peso) + 486.6;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        } else if(edad<60){
            calorias = (8.126*peso) + 845.6;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        } else if(edad>=60){
            calorias = (9.082*peso) + 658.5;
            calorias = calorias.toFixed(0);
            caloins = calorias+" calorias";
        }
    }
    document.getElementById("txtCalorias").value = caloins; 
}
