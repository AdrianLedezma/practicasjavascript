
//Declaracion de arreglos
var arreglo = [19, 20, 3, 2, 6, 3, 3, 9];

//Mostrar los elementos de arreglos
function MostrarArreglo(arreglo){
    for (let con = 0; con < arreglo.length; ++con){
        console.log(con + ": " + arreglo[con]);
    }
}

//Funcion que regrese el promedio de los elementos del arreglo
function PromedioArreglo(arreglo){
    let prom = 0;
    for (let con = 0; con < arreglo.length; ++con){
        prom = prom + arreglo[con];
    }
    prom = prom / arreglo.length;
    return prom;
}

//Funcion que regrese un valor entero que represente la cantidad 
//de numeros pares que esta en el arreglo

function ContarPares(arreglo){ 
    let pares = 0;
    for (let i = 0; i < arreglo.length; ++i){
        if (arreglo[i] % 2 == 0){
            pares++;
        }
    }
    return pares;
}

//Funcion que regresa el valor mayor contenido en el arreglo

function EncontrarMayor(arreglo){
    let mayor = arreglo[0];

    for (let i = 1; i < arreglo.length; i++){
        if (arreglo[i] > mayor) {
            mayor = arreglo[i];
        }
    }
    return mayor;
}

MostrarArreglo(arreglo);
var promedio = PromedioArreglo(arreglo);
var cantidadPares = ContarPares(arreglo);
var mayor = EncontrarMayor(arreglo);
console.log("Promedio: " + promedio);
console.log("Cantidad de pares: " + cantidadPares);
console.log("Mayor valor: " + mayor);

function randomize(x){
    const cmbAleatorios = document.getElementById('cmbNumeros');

    let arr = [];
    for(con=0;con<x;con++){
        arr[con] = Math.floor(Math.random()*1000);
        let option = document.createElement('option');
        option.value = arr[con];
        option.innerHTML = arr[con];
        cmbAleatorios.appendChild(option);
        
    }
}