function GenerarAccion(){

    LimpiarResultados();

    const num = document.getElementById('numGen').value;

    if (num < 1) {
        alert('Ingresa un valor mayor a 0.');
        return;
    }

    const numeros = GenerarNumerosAleatorios();
  
    MostrarNumerosCmb(numeros);
    CalcularPromedio(numeros);
    ObtenerNumeroMayorYPosicion(numeros);
    ObtenerNumeroMenorYPosicion(numeros);
    CalcularPorcentajeParesImparesSimetria(numeros);
  }
  
  function GenerarNumerosAleatorios() {
    const num = document.getElementById('numGen').value;
  
    let arreglo = [];
    for (let con = 0; con < num; con++) {
      arreglo[con] = Math.floor(Math.random() * 1000);
    }
    return arreglo;
  }
  
  
  function MostrarNumerosCmb(numeros) {
    const select = document.getElementById('cmbNumeros');
    for (const numero of numeros) {
      const option = document.createElement('option');
      option.value = numero;
      option.text = numero;
      select.appendChild(option);
    }
  }
  
  function CalcularPromedio(numeros) {
    if (numeros.length === 0) {
      return 0;
    }
    let suma = 0;
    for (let i = 0; i < numeros.length; i++) {
      suma += numeros[i];
    }
    promedio = suma / numeros.length;
  
    const Prom = document.querySelector('label[for="lblPromedio"]');
    Prom.textContent = `Promedio: ${promedio.toFixed(2)}`;
  }
  
  function ObtenerNumeroMayorYPosicion(numeros) {
  
    let numeroMayor = numeros[0];
    let posicion = 0;
  
    for (let i = 1; i < numeros.length; i++) {
      if (numeros[i] > numeroMayor) {
        numeroMayor = numeros[i];
        posicion = i;
      }
    }
    const MayorInd = document.querySelector('label[for="lblMayorInd"]');
    MayorInd.textContent = `Mayor: ${numeroMayor}, Índice: ${posicion}`;
  }
  
  function ObtenerNumeroMenorYPosicion(numeros) {
  
    let numeroMenor = numeros[0];
    let posicion = 0;
  
    for (let i = 1; i < numeros.length; i++) {
      if (numeros[i] < numeroMenor) {
        numeroMenor = numeros[i];
        posicion = i;
      }
    }
    const MenorInd = document.querySelector('label[for="lblMenorInd"]');
    MenorInd.textContent = `Menor: ${numeroMenor}, Índice: ${posicion}`;
  }
  
  function CalcularPorcentajeParesImparesSimetria(numeros) {
    let pares = 0;
    let impares = 0;
  
    for (const numero of numeros) {
      if (numero % 2 === 0) {
        pares++;
      } else {
        impares++;
      }
    }
  
    const total = pares + impares;
    const porcentajePares = (pares / total) * 100;
    const porcentajeImpares = (impares / total) * 100;
  
    const Pares = document.querySelector('label[for="lblPorcentajePares"]');
    Pares.textContent = `Porcentaje de pares: ${porcentajePares.toFixed(2)}%`;
  
    const Impares = document.querySelector('label[for="lblPorcentajeImpares"]');
    Impares.textContent = `Porcentaje de impares: ${porcentajeImpares.toFixed(2)}%`;
  
    const Simetrialbl = document.querySelector('label[for="lblSimetrico"]');
  
  
    let simetria = porcentajePares - porcentajeImpares;
  
    if (simetria <= 20){
      Simetrialbl.textContent = `Simétrico: Sí`
    }else{
      Simetrialbl.textContent = `Simétrico: No`
    }
  }

  function LimpiarResultados() {

    const cmbNumeros = document.getElementById('cmbNumeros');
    while (cmbNumeros.firstChild) {
      cmbNumeros.removeChild(cmbNumeros.firstChild);
    }
    const lblPromedio = document.querySelector('label[for="lblPromedio"]');
    lblPromedio.textContent = 'Promedio:';

    const lblMayorInd = document.querySelector('label[for="lblMayorInd"]');
    lblMayorInd.textContent = 'Mayor:, Índice:';
  
    const lblMenorInd = document.querySelector('label[for="lblMenorInd"]');
    lblMenorInd.textContent = 'Menor:, Índice:';
  
    const lblPorcentajePares = document.querySelector('label[for="lblPorcentajePares"]');
    lblPorcentajePares.textContent = 'Porcentaje de pares:';
  
    const lblPorcentajeImpares = document.querySelector('label[for="lblPorcentajeImpares"]');
    lblPorcentajeImpares.textContent = 'Porcentaje de impares:';
  
    const lblSimetrico = document.querySelector('label[for="lblSimetrico"]');
    lblSimetrico.textContent = 'Simétrico:';
  }