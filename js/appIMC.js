const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click', function(){
    let valorAltura = document.getElementById('txtAltura').value;
    let valorPeso = document.getElementById('txtPeso').value;

    let resultado = valorPeso / (valorAltura * valorAltura);

    resultado = resultado.toFixed(2);

    document.getElementById('txtResultado').value = resultado;
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtAltura').value = "";
    document.getElementById('txtPeso').value = "";
    document.getElementById('txtResultado').value = "";
});